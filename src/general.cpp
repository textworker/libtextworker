/*
    (C) 2024 Le Bao Nguyen and contributors.
    This is a part of the libtextworker project, licensed under the GNU General Public License version 3.0 or later.
*/

#include <libtextworker/general.h>
#include <libtextworker/pch.h>
#include <fstream>

#ifndef __WIN32__
    #include <pwd.h>
    #include <unistd.h>
#endif

namespace libtextworker::general
{
    std::string CraftItems(std::vector<const char *> args)
    {
        std::filesystem::path p = std::filesystem::path();
        for (const char* arg: args) { p /= arg; }
        return p.string();
    }

    int CreateDirectory(char *directory, std::vector<const char *> childs)
    {
        char *sub;
        char *seps[] = {"/", "\\\\"}; // is this good for Windows paths that use just one slash?

        try
        {
            std::filesystem::create_directory(directory);
        }
        catch (std::filesystem::filesystem_error const &ex)
        {
            throw libTewException(ex.what());
        }

        for (const char *child : childs)
        {
            for (char *sep : seps)
            {
                sub = strstr(child, sep);
                if (sub != NULL)
                {
                    throw libTewException(
                        "Found path separator in CreateDirectory.childs - not allowed!\n"
                        "Please use WalkCreation instead.");
                }
                else
                {
                    std::filesystem::create_directory(strcat(directory, child));
                }
            }
        }

        return 0;
    }

    int WalkCreation(const char *path)
    {
        return std::filesystem::create_directories(path);
    }

    int exists(const char* path)
    {
        if (std::filesystem::exists(path)) {
            if (std::filesystem::is_directory(path))
                return 1;
            return 0;
        }
        return -1;
    }

    const char* getHomePath()
    {
    #ifdef __WIN32__
        return getenv("USERPROFILE"); // Is this variable any have problems?
    #else
        struct passwd *pw = getpwuid(getuid());
        return pw->pw_dir;
    #endif
    }
}