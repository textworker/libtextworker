#include <regex>
#include <libtextworker/interface/wxGUI/miscs.h>

namespace libtextworker::UI::wx
{
    /* Creates a wxMenu with a list of tuples:
       (id, label, helptext, handler, kind)
       No sub-menu support. Events (wxEVT_MENU) are automatically bond to the window. */
    wxMenu* MakeMenu(wxWindow* parent, std::vector<TMenuItem> items)
    {
        wxMenu* target;

        for (TMenuItem item: items) {
            parent->Bind(wxEVT_MENU, item.handler, item.id, -1, target->Append(item.id, item.label, item.helptext, item.kind));
        };

        return target;
    };

    template <class TWind, class EventType>
    /* Binds wxEVT_MENU events an easier and more convenient way. Ideal for XRC menus. */
    void BindMenuEvents(TWind* window, wxMenu* menu, std::vector<std::tuple<std::function<void(EventType&)>, int>> items)
    {
        std::function<void(EventType)> callback;
        int position;

        for (std::tuple item: items)
        {
            std::tie(callback, position) = item;
            window->Bind(wxEVT_MENU, &callback, menu->FindItemByPosition(position));
        }
    };

    template <class TWind, class EventType>
    void BindEvents(TWind* window, std::vector<std::tuple<wxEvent, std::function<void(EventType)>, int>> items)
    {
        wxEvent event;
        std::function<void(EventType)> callback;
        int position;

        for (std::tuple item: items)
        {
            std::tie(event, callback, position) = item;
            window->Bind(event, callback, window->GetChildren()[position]);
        }
    };

    template <class GetType, class ReturnType>
    XMLBuilder::XMLBuilder(wxWindow* parent, char filePath, std::function<GetType(ReturnType)> transFunc) {
        Parent = parent;
        translateFunc = transFunc;

        std::fstream fileReadWrite(filePath);
        std::stringstream fileContent;
        fileContent << fileReadWrite.rdbuf();

        std::regex re("_(['\"](.*?)['\"])");
        
        fileContent.str(std::regex_replace(fileContent, re, [transFunc](const std::smatch& m) {
            return transFunc(m.str()); // not tested, the entire project is not tested;-)
        }));

        wxFileSystem::AddHandler(new wxMemoryFSHandler);
        wxMemoryFSHandler::AddFile(filePath, fileContent.str());

        Res = wxXmlResource::Get();
        Res->Load("memory:" + filePath);
    };

        /* Returns a XRC object. */
    wxObject* XMLBuilder::loadObject(const char* objectname, const char* objecttype) {
        return Res->LoadObjectRecursively(this->Parent, objectname, objecttype);
    };

}