/*
    (C) 2024 Le Bao Nguyen and contributors.
    This is a part of the libtextworker project, licensed under the GNU General Public License version 3.0 or later.
*/

#include <libtextworker/get_config.h>
#include <libtextworker/general.h>
#include <optional>

using namespace libtextworker::get_config;
using namespace libtextworker::general;

GetConfig::GetConfig(GCfgDictionary OEMconfig, std::string targetfile, bool useJSONPLS)
{
    Create(OEMconfig, targetfile, useJSONPLS);
}

void GetConfig::Create(GCfgDictionary OEMconfig, std::string targetfile, bool useJSONPLS)
{
    if (useJSONPLS)
        #ifdef JSON_SUPPORT
        readJSON(targetfile);
        #else
        throw libTewException("Attempting to read JSON while libtextworker is not compiled for doing so.\n"
                                "Add -DJSON_SUPPORT to the compiler.");
        #endif
    else
        #ifdef INI_SUPPORT
        readINI(targetfile);
        #else
        throw libTewException("Attempting to read INI while libtextworker is not compiled for doing so.\n"
                                "Add -DINI_SUPPORT to the compiler.");
        #endif

    OEMSettings = OEMconfig;

    for (std::string yes : true_values)
        aliases.insert({yes, true});

    for (std::string no : false_values)
        aliases.insert({no, false});
}

void GetConfig::Reset(bool restore)
{   
    Restore(OEMSettings);

    if (restore && !backups.empty()) {
        Restore(backups);
    }

    WriteBack();
}

GCfgDictionary
GetConfig::Backup(GCfgDictionary keys, bool direct_to_keys)
{
    #ifdef INI_SUPPORT
    if (!useJSON)
    {
        for (const auto &[key, value] : INIreader) {
            if (direct_to_keys)
                keys[key] = value;
            else
                backups[key] = value;
        }
    }
    #endif

    #ifdef JSON_SUPPORT
    if (useJSON)
    {
        if (direct_to_keys)
        {
            for (Json::ValueIterator it = JSONreader.begin(); it != JSONreader.end(); ++it)
                keys[it.key().asString()] = JSONreader[it.key().asString()];
        }
    }
    #endif

    return direct_to_keys ? keys : backups;
}

void GetConfig::Restore(GCfgDictionary keys)
{
    for (const auto &[key, value]: keys)
    {
        #ifdef INI_SUPPORT
        if (!useJSON)
            INIreader.set(key, std::any_cast<mINI::INIMap<std::string>>(value));
        #endif

        #ifdef JSON_SUPPORT
        if (useJSON)
            JSONreader[key] = std::any_cast<Json::Value>(value);
        #endif
    }
}

void GetConfig::WriteBack(std::string path)
{
    #ifdef INI_SUPPORT
    if (!useJSON)
        mINI::INIFile(path).write(INIreader, true);
    #endif

    #ifdef JSON_SUPPORT
    if (useJSON)
        std::ofstream(path) << JSONreader;
    #endif
}

void GetConfig::WriteBack()
{
    WriteBack(_file);
}

std::any GetConfig::Get(std::string section, std::string option,
                        bool needed, bool make, bool noRaise, bool raw)
{
    GCfgDictionary target = backups;

    if (target.empty())
    { target = OEMSettings; }

    // TODO: check if target variable above has section+option as specified
    // If not, the entire function will return NULL.

    // Finds the option in the reader. If roRaise and make are false, raise an exception if no section/option.
    // Else: noRaise and make are true, create section and option if no such.
    // Returns the requested value. If not able to, do nothing.
    std::string get = [=]() {
        // TODO: Exception class
        #ifdef INI_SUPPORT
        if (!useJSON)
        {
            if (needed)
            {
                // TODO: Fix this stupid code
                if (!INIreader.has(section))
                {
                    // looks so stupid
                    if (!noRaise)
                        throw libTewException(_file, ": Section", section, "not found");
                    else
                        if (make)
                            INIreader[section] = std::any_cast<mINI::INIMap<std::string>>(target.at(section));
                        else
                            return std::any_cast<mINI::INIMap<std::string>>(target.at(section))[option];
                }

                else if (!INIreader[section].has(option))
                {
                    if (!noRaise)
                        throw libTewException(_file, "->", section, ": Option", option, "not found");
                    else
                        if (make)
                            INIreader[section][option] = std::any_cast<mINI::INIMap<std::string>>(target.at(section))[option];
                        else
                            return std::any_cast<mINI::INIMap<std::string>>(target.at(section))[option];
                }
                return INIreader[section][option];
            }
        }
        #endif

        #ifdef JSON_SUPPORT
        if (useJSON)
            if (needed)
            {
                if (!JSONreader[section])
                {
                    if (!noRaise)
                        throw libTewException(_file, ": Section", section, "not found");
                    else
                        if (make)
                            JSONreader[section] = std::any_cast<Json::Value>(target.at(section));
                        else
                            return std::any_cast<Json::Value>(target.at(section))[option].asString();
                }

                else if (!JSONreader[section][option])
                {
                    if (!noRaise)
                        throw libTewException(_file, "->", section, ": Option", option, "not found");
                    else
                        if (make)
                            JSONreader[section][option] = std::any_cast<Json::Value>(target.at(section))[option];
                        else
                            return std::any_cast<Json::Value>(target.at(section))[option].asString();
                }
                return JSONreader[section][option].asString();
            }
        #endif
    }();

    if (raw || (aliases.count(get) == 0))
        return get;
    else {
        std::string_view result = {get};
        const char substrs[] = {'\'', '\"'};
        for (char substr : substrs)
        {
            if (result[0] == substr)
                result.remove_prefix(substr);
            if (result[result.length() - 1] == substr)
                result.remove_suffix(substr);
        }
        return aliases[get];
    }
}

void GetConfig::Set(std::string section, std::string option, std::any value)
{
    #ifdef INI_SUPPORT
    if (!useJSON)
        INIreader[section].set(option, std::any_cast<std::string>(value));
    #endif

    #ifdef JSON_SUPPORT
    if (useJSON)
        JSONreader[section][option] = std::any_cast<Json::Value>(value);
    #endif
}

void GetConfig::Alias(std::string from, std::any to)
{
    aliases.insert({from, to});
}

void GetConfig::AliasYesNo(std::string yesValue, std::string noValue)
{
    if (!yesValue.empty()) // will this work with nullopt?
    {
        true_values.push_back(yesValue);
        Alias(yesValue, true);
    }
    
    if (!noValue.empty())
    {
        false_values.push_back(noValue);
        Alias(noValue, false);
    }
}

void GetConfig::Move(std::map<std::string, std::map<std::string, std::string>> dictionary)
{
    // not implemented
}