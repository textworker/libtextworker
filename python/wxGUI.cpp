#include "libtextworker_python.h"
#include <libtextworker/UI.h>
#include <libtextworker/interface/wxGUI.h>
#include <libtextworker/interface/wxGUI/about.h>
#include <libtextworker/interface/wxGUI/actionrow.h>
#include <libtextworker/interface/wxGUI/editor.h>
#include <libtextworker/interface/wxGUI/miscs.h>

using namespace libtextworker::UI;
using namespace libtextworker::UI::wx;

void add_wxUpLevel()
{
    py::class_<wxColorManager, ColorManager>(wxGUIModule, "wxColorManager")
        .def(py::init<>())
        .def_readwrite("recursive_configure", &wxColorManager::recursive_configure)
        .def("GetFont", &wxColorManager::GetFont)
        .def("Configure", &wxColorManager::Configure);
    
    wxGUIModule.attr("FONTWT") = FONTWT;
    wxGUIModule.attr("FONTST") = FONTST;
    wxGUIModule.attr("FONTSZ") = FONTSZ;
}

template <class GetType, class ReturnType, class TWind, class EventType>
void add_wxMiscs()
{
    auto s = wxGUIModule.def_submodule("miscs", "Misc wxPython functions")
        .def("MakeMenu", &MakeMenu, "Creates a wxMenu with a list of tuples.")
        .def("BindMenuEvents", &BindMenuEvents, "Bind wxEVT_MENU events an easier way.")
        .def("BindEvents", &BindEvents);
    
    py::class_<TMenuItem>(s, "TMenuItem")
        .def_readwrite("id", &TMenuItem::id)
        .def_readwrite("label", &TMenuItem::label)
        .def_readwrite("helptext", &TMenuItem::helptext)
        .def_readwrite("handler", &TMenuItem::handler)
        .def_readwrite("kind", &TMenuItem::kind);

    py::class_<XMLBuilder>(s, "XMLBuilder")
        .def(py::init<wxWindow*, char, std::function<GetType(ReturnType)>>())
        .def("loadObject", &XMLBuilder::loadObject)
        .def_readwrite("Parent", &XMLBuilder::Parent)
        .def_readwrite("Res", &XMLBuilder::Res)
        .def_readwrite("translateFunc", &XMLBuilder::translateFunc);
}

template <class TEvent>
void add_wxAbout()
{
    py::class_<AboutDialog>(
        wxGUIModule.def_submodule(
            "about",
            "About dialog for wxPython projects"
        ),
        "AboutDialog"
    )
        .def("SetArtists", &AboutDialog::SetArtists)
        .def("SetCopyRight", &AboutDialog::SetCopyRight)
        .def("SetDescription", &AboutDialog::SetDescription)
        .def("SetDevelopers", &AboutDialog::SetDevelopers)
        .def("SetDocWriters", &AboutDialog::SetDocWriters)
        .def("SetIcon", &AboutDialog::SetIcon)
        .def("SetName", &AboutDialog::SetName)
        .def("SetTranslators", &AboutDialog::SetTranslators)
        .def("SetVersion", &AboutDialog::SetVersion)
        .def("SetWebSite", &AboutDialog::SetWebSite)
        .def("SetLicense", &AboutDialog::SetLicence)
        .def("ShowBox", py::overload_cast<>(&AboutDialog::ShowBox))
        .def("ShowBox", py::overload_cast<TEvent>(&AboutDialog::ShowBox));
}