#include "libtextworker_python.h"
#include <libtextworker/libtextworker.h>

using namespace libtextworker;
using namespace py::literals;

PYBIND11_MODULE(libtextworker, m)
{
    m.attr("TOPLV_DIR") = TOPLV_DIR;
    m.attr("__version__") = __version__;

    generalModule = m.def_submodule("general", "Several, uncatalogized utilities");
    getconfigModule = m.def_submodule("get_config", "Home of GetConfig class - JSON and INI reader");
    UIModule = m.def_submodule("get_config", "Home of GetConfig class - JSON and INI reader");
    versioningModule = m.def_submodule("versioning", "Project versioning functions");

    wxGUIModule = UIModule.def_submodule("wx", "wxPython support");

    add_general();
    add_getconfig();
    // add_versioning();
    add_UI();
    add_wxUpLevel();
    add_wxAbout();
}