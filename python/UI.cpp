#include <libtextworker/UI.h>
#include <libtextworker/get_config.h>
#include "libtextworker_python.h"

using namespace libtextworker::UI;
using namespace libtextworker::get_config;

void add_UI()
{
    py::class_<ColorManager, GetConfig>(UIModule, "ColorManager")
        .def(py::init<>())
        
        .def("GetColors", &ColorManager::GetColors)
        .def("GetFont", &ColorManager::GetFont)
        ;
}