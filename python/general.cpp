#include "libtextworker_python.h"
#include <libtextworker/general.h>

using namespace libtextworker::general;

void add_general() {
    py::register_exception<libTewException&>(generalModule, "libTewException");
    generalModule.def("CraftItems", &CraftItems, "Crafts any >= 2 paths, together.");
    generalModule.def("CreateDirectory", &CreateDirectory, "Creates a directory with subfolders inside (optional).");
    generalModule.def("WalkCreation", &WalkCreation, "The same as CreateDirectory, but with deeper for a fast setup.");
    generalModule.def("exists", &exists, "Checks if a path exists as a file or folder.");
    generalModule.def("getHomePath", &getHomePath, "Gets the user home folder.");
}