#pragma once
#include <pybind11/pybind11.h>

namespace py = pybind11;

void add_getconfig();
void add_general();
void add_UI();
void add_versioning();

// wx bindings

void add_wxUpLevel();
void add_wxAbout();

// modules

py::module_ generalModule;
py::module_ getconfigModule;
py::module_ UIModule;
py::module_ versioningModule;
py::module_ wxGUIModule;
