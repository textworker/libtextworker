#include "libtextworker_python.h"
#include <libtextworker/get_config.h>

using namespace libtextworker::get_config;

void add_getconfig()
{
    py::class_<GetConfig>(getconfigModule, "GetConfig")
        .def(py::init<>())
        .def(py::init<GCfgDictionary, std::string, bool>())
        .def("__getitem__", &GetConfig::operator[])
        .def("Create", &GetConfig::Create)

        .def("Reset", &GetConfig::Reset)
        .def("Backup", &GetConfig::Backup)
        .def("Restore", &GetConfig::Restore)
        .def("WriteBack", py::overload_cast<std::string>(&GetConfig::WriteBack))
        .def("WriteBack", py::overload_cast<>(&GetConfig::WriteBack))
        .def("Get", &GetConfig::Get)
        .def("Alias", &GetConfig::Alias)
        .def("AliasYesNo", &GetConfig::AliasYesNo)
        .def("Move", &GetConfig::Move)
        .def("SetAndUpdate", &GetConfig::SetAndUpdate)
        .def("getSectionForINI", &GetConfig::getSectionForINI)
        .def("getSectionForJSON", &GetConfig::getSectionForJSON)

        .def_readwrite("useJSON", &GetConfig::useJSON)
        .def_readwrite("true_values", &GetConfig::true_values)
        .def_readwrite("false_values", &GetConfig::false_values)
        .def_readwrite("aliases", &GetConfig::aliases)
        .def_readonly("backups", &GetConfig::backups);
}