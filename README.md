## libtextworker

An universal, cross-platform library for C/C++ and Python projects, mainly targets GUI apps.

Originally made in pure Python, and this C++ port has things that Python one doesn't have, hasn't things that Python one has.

* Restructed: no `libtextworker::interface` namespace (`libtextworker.interface` in Python), `UI` is the new name. There are also some replacements in GUI scope. Some non-preimplemented functions (like Python's `hashlib.md5`, needs a function for make and simplify the progress) are implemented under `libtextworker::general`.

* SIP: This is for Python, not yet made.

* C-style string: `std::string` will be used mostly.

* Macros: enables more build options

## Build steps

Coming soon, and this does not have a build system like Makefile or Meson for now.

You will need:
* PyBind11 for Python bindings (optional)
* wxWidgets (optional, should be configurable later)
* Git
* Meson
* C++ 17-compatible compiler

Tkinter support is still in pure Python, will be here after the 0.1.4 release.

## About autocolor in wxWidgets

wxWidgets since the latest version, 3.3.0 for now, has a new appearance system which is quite useful and reduces the need for remaking `darkdetect`, and this library only supports one more library: Tkinter, pure Python. Usable for cross platform.

(Maybe add Qt in the future? But now I have so much things to do.)

wxPython does not have this, but automated/nightly builds **may**.

All related classes are documented and clearly available on wxWidgets **3.3.0**.