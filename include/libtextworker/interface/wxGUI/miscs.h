#pragma once

/* FIXME: Reorder this */
#include <wx/menu.h>
#include <wx/xrc/xmlres.h>
#include <wx/filesys.h>
#include <wx/fs_mem.h>

#include <functional>
#include <libintl.h>
#include <libtextworker/interface/wxGUI.h>

namespace libtextworker::UI::wx
{
    struct TMenuItem
    {
        wxStandardID id;
        wxString label;
        wxString helptext;
        auto handler = [](wxCommandEvent&) {};
        wxItemKind kind = wxITEM_NORMAL;
    };

    /* Creates a wxMenu with a list of tuples:
       (id, label, helptext, handler, kind)
       No sub-menu support. Events (wxEVT_MENU) are automatically bond to the window. */
    wxMenu* MakeMenu(wxWindow* parent, std::vector<TMenuItem> items);

    template <class TWind, class EventType>
    /* Binds wxEVT_MENU events an easier and more convenient way. Ideal for XRC menus. */
    void BindMenuEvents(wxWindow* window, wxMenu* menu, std::vector<std::tuple<std::function<void(EventType&)>, int>> items);

    template <class TWind, class EventType>
    void BindEvents(wxWindow* window, std::vector<std::tuple<wxEvent, std::function<void(EventType)>, int>> items);

    /* Class to read wxXRC UIs. */
    class XMLBuilder
    {
    public:
        wxWindow* Parent;
        wxXmlResource* Res;
        std::function<char*(const char*)> translateFunc;

        template <class GetType, class ReturnType>
        XMLBuilder(wxWindow* parent, char filePath, std::function<GetType(ReturnType)> transFunc = &gettext);

        /* Returns a XRC object. */
        wxObject* loadObject(const char* objectname, const char* objecttype);
    };

}