#pragma once

#include <wx/stc/stc.h>
#include <wx/dnd.h>

#include <libtextworker/libtextworker.h>
#include <libtextworker/general.h>
#include <libtextworker/get_config.h>
#include <libtextworker/interface/wxGUI/miscs.h>
#include <libtextworker/UI.h>

namespace libtextworker::UI
{
    namespace wx {

        class StyledTextControl: public wxStyledTextCtrl, wxFileDropTarget, wxTextDropTarget
        {
        public:
            // GetConfig cfg;

            StyledTextControl(wxWindow* parent, wxWindowID id=wxID_ANY,
                              const wxPoint& pos=wxDefaultPosition, const wxSize& size=wxDefaultSize,
                              long style = 0, const wxString& name=wxSTCNameStr)
                : wxStyledTextCtrl(parent, id, pos, size, style, name) {}

                
            bool OnDropText(wxCoord x, wxCoord y, const wxString& text) { WriteText(text); };
            bool OnDropFiles(wxCoord x, wxCoord y, const wxArrayString& filenames) {
                LoadFile(filenames[0]);
            };

            void EditorInit(char* configPath = "")
            {
                // if (configPath != "")
                //     configPath = CraftItems({EDITOR_DIR, "default.ini"});
                
                // cfg = GetConfig(stock_editor_configs, configPath);

                LineNumbers();
                DNDSupport();
                IndentationSet();

                // if (std::find(cfg.true_values.begin(), cfg.true_values.end(), cfg.Get("menu", "enabled")) != cfg.true_values.end())
                //     Bind(wxEVT_RIGHT_DOWN, StyledTextControl::MenuPopup, this);
            };

            bool DNDSupport() {
                // if (std::find(cfg.true_values.begin(), cfg.true_values.end(), cfg.Get("editor", "dnd_enabled")) != cfg.true_values.end())
                // {
                    // DragNDropTarget* dnd = new DragNDropTarget(this);
                    // SetDropTarget(dnd);
                //     return true;
                // }
                return false;
            };

            void IndentationSet() {
                // int size = std::any_cast<int>(cfg.Get("indentaion", "size"));
                // char* tp = std::any_cast<char*>(cfg.Get("indentation", "type"));
                // char* show_guide = std::any_cast<char*>(cfg.Get("indentation", "show_guide"));
                // char* bk_unindent = std::any_cast<char*>(cfg.Get("indentation", "backspace_unindents"));
                // char* view_ws = std::any_cast<char*>(cfg.Get("editor", "view_whitespaces"));

                // SetUseTabs(tp == "tabs");
                // SetBackSpaceUnIndents(std::find(cfg.true_values.begin(), cfg.true_values.end(), bk_unindent) != cfg.true_values.end());
                // SetViewWhiteSpace(std::find(cfg.true_values.begin(), cfg.true_values.end(), view_ws) != cfg.true_values.end());
                // SetIndent(size);
                // SetIndentationGuides(std::find(cfg.true_values.begin(), cfg.true_values.end(), show_guide) != cfg.true_values.end());
            };

            bool LineNumbers() {
                // char* state = std::any_cast<char*>(cfg.Get("editor", "line_count"));
                // if (std::find(cfg.true_values.begin(), cfg.true_values.end(), state) != cfg.true_values.end())
                // {
                //     SetMarginWidth(0, 0);
                //     return false;
                // }

                SetMarginWidth(0, 20);
                SetMarginType(0, wxSTC_MARGIN_NUMBER);
                SetMarginMask(0, 0);
                Bind(wxEVT_STC_UPDATEUI, &StyledTextControl::OnUIUpdate, this);
                return true;
            };

            void OnUIUpdate(wxStyledTextEvent& event)
            {
                int line_count = GetLineCount();
                int margin_width;
                int last_line_width;

                if (line_count <= 1000)
                {
                    margin_width = 40;
                }
                else
                {
                    last_line_width = TextWidth(wxSTC_STYLE_LINENUMBER, std::any_cast<char*>(line_count));
                    margin_width = last_line_width + 4;
                }
                
                SetMarginWidth(0, margin_width);
                event.Skip();
            }

            void MenuPopup(wxMouseEvent& event) {
                wxPoint pt = event.GetPosition();
                
                TMenuItem cutItem;
                cutItem.handler = [this](wxCommandEvent&){ Cut(); };
                cutItem.id = wxID_CUT;
                cutItem.label = wxEmptyString;
                cutItem.helptext = wxEmptyString;

                TMenuItem copyItem;
                copyItem.handler = [this](wxCommandEvent&){ Copy(); };
                copyItem.id = wxID_COPY;
                copyItem.label = wxEmptyString;
                copyItem.helptext = wxEmptyString;

                TMenuItem pasteItem;
                TMenuItem separator;
                TMenuItem undoItem;
                TMenuItem redoItem;

                wxMenu* menu = MakeMenu(this, {cutItem, copyItem});
                PopupMenu(menu, pt);
            };
        };
    };
}