#pragma once

#include <libtextworker/UI.h>
#include <libtextworker/pch.h>
#include <wx/font.h>
#include <wx/wx.h>

namespace libtextworker::UI
{
    namespace wx
    {
        static std::map<std::string, int> FONTWT = {
            {"light", wxFONTWEIGHT_LIGHT},
            {"normal", wxFONTWEIGHT_NORMAL},
            {"semibold", wxFONTWEIGHT_SEMIBOLD},
            {"bold", wxFONTWEIGHT_BOLD},
            {"maxlight", wxFONTWEIGHT_EXTRALIGHT},
            {"maxbold", wxFONTWEIGHT_EXTRABOLD}
        };

        static std::map<std::string, int> FONTST = {
            {"normal", wxFONTSTYLE_NORMAL},
            {"italic", wxFONTSTYLE_ITALIC}
        };

        static std::map<std::string, int> FONTSZ = {
            {"normal", 12},
            {"small", 8},
            {"large", 16}
        };

        class wxColorManager: public ColorManager
        {
        public:
            using ColorManager::ColorManager;

            bool recursive_configure = true;

            /* Returns a wxFont object based on current settings. */
            wxFont GetFont();

            /* Configures a wx control.
               By default this will get the current color scheme, and apply needed changes
               not only to the target itself, but also its childrens. */
            void Configure(wxWindow* widget, char* color = "", bool recursivelly = true);
        };
    };
}

