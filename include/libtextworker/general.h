#pragma once

#include <exception>
#include <vector>
#include <stdarg.h>
#include <openssl/md5.h>
#include <sstream>

namespace libtextworker
{
    namespace general
    {
        /* The base class for libtextworker exceptions. */
        class libTewException : public std::exception
        {
        private:
            std::string message;

        public:
            libTewException(const char* msg) : message(msg) {}

            template <class ... Args>
            libTewException(std::string firstarg, Args... args) {
                std::ostringstream oss;
                ((oss << firstarg << " " << args), ...);
                message = oss.str();
            }

            const char* what() { return message.c_str(); }
        };

        /* Crafts any >=2 path, together.
           @param args (std::vector of char*): Strings to use.
           @return The generated path.
        */
        std::string CraftItems(std::vector<const char *> args);

        /* Creates a directory with subfolders inside (optional).
           @param directory (char*): Target parent directory
           @param childs (std::vector of char*): Folder childrends
           @return Status of the progress. 0 is success.
           @throws libTewException: On directory create error
        */
        int CreateDirectory(char *directory, std::vector<const char *> childs = {});

        /* Creates a directory with subfolders inside, layer-to-layer.
           @paragraph For example: path1/path2/path3. This function will create path1, path1/path2, then path1/path2/path3. Skip existing folders.
           @param path (char*): Target folder path.
           @return Status of the progress. 0 is success.
        */
        int WalkCreation(const char *path);

        /* Check if a file or folder exists.
           @return 0 if the path exits as a file, 1 if as a directory. -1 if the path does not exists on the file system.
        */
        int exists(const char* path);

        const char* getHomePath();
    }
}