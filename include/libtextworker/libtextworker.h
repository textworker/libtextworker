#pragma once

/*
    An universal, cross-platform library mainly targets GUI applications!
    Now available as a Pypi package and C++ library.
*/
namespace libtextworker {
    static const char* __version__ = "1.0alpha"; // Project version.
    static const char* TOPLV_DIR;
}