#pragma once

#include <libtextworker/pch.h>
#include <libtextworker/get_config.h>

namespace libtextworker
{
    /*
        libtextworker namespace for GUIs.
    */
    namespace UI
    {
        class ColorManager: public libtextworker::get_config::GetConfig {
        public:
            using libtextworker::get_config::GetConfig::GetConfig;
            // std::tuple<std::string, std::string> GetColors();
            /*
                Gets the color from current system scheme
                Warning: no darkdetect for C++ right now.
            */
            std::tuple<std::string, std::string> GetColors(std::string scheme);

            /* Gets current font settings. */
            std::tuple<int, std::string, std::string, std::string> GetFont();

            /* Configures a GUI widget, setting its colors.
               By default in libtextworker::UI::ColorManager Configure won't do anything. */
            void Configure(auto* widget, std::string colorScheme = "") {};
        };
    }
}