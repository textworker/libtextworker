#include <any>
#include <map>
#include <string>

namespace libtextworker
{
    namespace versioning
    {
        std::map<char*, std::any> Requested;
        char* VERSION_PATTERN;

        class VersioningException : public std::exception
        {
        protected:
            std::string message;

        public:
            VersioningException(std::string msg) : message(msg) {}

            template <class ... Args>
            VersioningException(std::string firstarg, Args... args) {
                std::ostringstream oss;
                ((oss << firstarg << " " << args), ...);
                message = oss.str();
            }

            const std::string what() { return message; }
        };

        class InvalidVersion : public VersioningException
        {
        public:
            InvalidVersion(std::string version) : VersioningException("Invalid version: " + version) {};

            // template <class ... Args>
            // InvalidVersion(std::string firstarg, Args)
        };

        class Version
        {
        public:
            Version(char* ver) {};

            const std::tuple<char*, int> pre;
            const bool isPrerelease = false;
            const bool isPostrelease = false;
            const bool isDevrelease = false;

            bool operator ==(Version* object) {};
            bool operator >(Version* object) {};
            bool operator <(Version* object) {};
            
            bool operator >=(Version* object) {
                return this > object || this == object;
            };

            bool operator <=(Version* object) {
                return (this < object) || (this == object);
            };
        };

        Version parse_version(char* name) {};
        
        bool is_development_version(char* version) { return Version(version).isDevrelease; };
        bool is_development_version_from_project(char* project) { return parse_version(project).isDevrelease; };

        void require(char* project, char* target_version) {};
        void require_exact(char* project, char* target_version) {};
        void require_lower(char* project, char* target_version) {};
    }
}